//
//  BookListPresenter.swift
//  Libreca
//
//  Created by Justin Marshall on 5/7/19.
//  
//  Libreca is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  Libreca is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with Libreca.  If not, see <https://www.gnu.org/licenses/>.
//
//  Copyright © 2019 Justin Marshall
//  This file is part of project: Libreca
//

import CalibreKit
import Foundation
import SQLite
import SwiftyDropbox

final class BookListPresenter: BookListPresenting {
    typealias View = BookListViewing
    
    private weak var view: View?
    private let router: BookListRouting
    private let interactor: BookListInteracting
    
    private var books: [BookFetchResult] = []
    
    var allBooks: [BookModel] {
        return books.compactMap { $0.book }
    }
    
    init(view: View, router: BookListRouting, interactor: BookListInteracting) {
        self.view = view
        self.router = router
        self.interactor = interactor
        
        NotificationCenter.default.addObserver(self, selector: #selector(sortSettingDidChange), name: Settings.Sort.didChangeNotification.name, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(urlDidChange), name: Settings.ContentServer.didChangeNotification.name, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(imageSettingDidChange), name: Settings.Image.didChangeNotification.name, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(updateBooks), name: Notifications.booksNeedUpdatedNotification.name, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(didReceiveMemoryWarning), name: UIApplication.didReceiveMemoryWarningNotification, object: nil)
    }
    
    // MARK: - BookListPresenting
    
    func fetchBooks(allowCached: Bool) {
        DispatchQueue(label: "com.marshall.justin.mobile.ios.Libreca.queue.presenter.fetchBooks", qos: .userInitiated).async {
            self.interactor.fetchBooks(allowCached: allowCached, start: { [weak self] result in
                DispatchQueue.main.async {
                    switch result {
                    case .success(let bookCount):
                        self?.view?.show(bookCount: bookCount)
                        
                        if bookCount == 0 {
                            self?.view?.show(message: "No books in library.")
                        }
                    case .failure(let error):
                        self?.handle(error)
                    }
                }
            }, progress: { [weak self] result, index in
                DispatchQueue.main.async {
                    switch result {
                    case .book(let book):
                        self?.view?.show(book: .book(book), at: index)
                    case .inFlight:
                        // this is created by the front end and so this should never happen
                        // at some point, refactor to make this unnecessary
                        break
                    case .failure(let retry):
                        self?.view?.show(book: .failure(retry: retry), at: index)
                    }
                }
            }, completion: { [weak self] results in
                guard let strongSelf = self else { return }
                strongSelf.books = results
                DispatchQueue.main.async {
                    self?.view?.reload(all: strongSelf.books)
                }
            })
            // swiftlint:disable:previous multiline_arguments_brackets
        }
    }
    
    func search(using terms: String, results: @escaping ([BookFetchResult]) -> Void) {
        let noResultsFoundMessage = "No results found. Try different search terms, separated by spaces."
        view?.show(message: "Searching...")
        guard !terms.isEmpty else {
            if books.isEmpty {
                view?.show(message: noResultsFoundMessage)
            } else {
                results(books)
            }
            return
        }
        
        DispatchQueue(label: "com.marshall.justin.mobile.ios.Libreca.queue.search.library.presenter", qos: .userInitiated).async { [weak self] in
            guard let strongSelf = self else { return }
            let matchResults = strongSelf.interactor.searchBooks(strongSelf.books, usingTerms: terms)
            
            DispatchQueue.main.async {
                if matchResults.isEmpty {
                    self?.view?.show(message: noResultsFoundMessage)
                } else {
                    results(matchResults)
                }
            }
        }
    }
    
    func authors(for book: BookModel) -> String {
        return book.authors.map { $0.name }.joined(separator: "; ")
    }
    
    func retryFailures() {
        interactor.retryFailures(
            from: books,
            progress: { [weak self] retriedResult, index in
                self?.books[index] = retriedResult
                switch retriedResult {
                case .book(let book):
                    self?.view?.show(book: .book(book), at: index)
                case .failure(let retry):
                    self?.view?.show(book: .failure(retry: retry), at: index)
                case .inFlight:
                    // this is created by the front end and so this should never happen
                    // at some point, refactor to make this unnecessary
                    break
                }
            }, completion: { [weak self] in
                guard let strongSelf = self else { return }
                DispatchQueue(label: "com.marshall.justin.mobile.ios.Libreca.queue.presenter.retryFailures.sort", qos: .userInitiated).async {
                    strongSelf.books = strongSelf.interactor.sort(books: strongSelf.books, by: Settings.Sort.current)
                    DispatchQueue.main.async {
                        strongSelf.view?.reload(all: strongSelf.books)
                    }
                }
            }
        )
    }
    
    func sort(by newSortSetting: Settings.Sort) {
        interactor.updateSortSetting(to: newSortSetting)
    }
    
    func fetchImage(for book: BookModel, completion: @escaping (UIImage) -> Void) {
        // plan to refactor this as part of https://gitlab.com/calibre-utils/Libreca/issues/336
        
        let imageEndpoint: (@escaping (Swift.Result<Image, FetchError>) -> Void) -> Void
        
        switch Settings.Image.current {
        case .thumbnail:
            imageEndpoint = book.fetchThumbnail
        case .fullSize:
            imageEndpoint = book.fetchCover
        }
        
        imageEndpoint { imageResult in
            switch imageResult {
            case .success(let image):
                completion(image.image)
            case .failure:
                completion(#imageLiteral(resourceName: "BookCoverPlaceholder"))
            }
        }
    }
    
    // MARK: - Notification handlers
    
    @objc
    private func sortSettingDidChange(_ notification: Notification) {
        // If books is empty, that means either:
        // a.) User has no books in library.
        // b.) There was an error fetching books.
        //
        // In either of these cases, updating the UI would clear out that empty
        // state or error message, which we don't want to
        // do. So just ignore the notification.
        guard !books.isEmpty else { return }
        
        books = interactor.sort(books: books, by: Settings.Sort.current)
        view?.reload(all: books)
    }
    
    @objc
    private func urlDidChange(_ notification: Notification) {
        view?.willRefreshBooks()
        fetchBooks(allowCached: false)
    }
    
    @objc
    private func imageSettingDidChange(_ notification: Notification) {
        interactor.clearImageCache()
    }
    
    @objc
    private func didReceiveMemoryWarning(_ notification: Notification) {
        interactor.clearImageCache()
    }
    
    @objc
    private func updateBooks(_ notification: Notification) {
        guard let updatedBooks = notification.userInfo?[Notifications.booksNeedUpdatedUserInfoKey] as? [BookModel] else {
            return
        }
        
        DispatchQueue(label: "com.marshall.justin.mobile.ios.Libreca.queue.presenter.updateBooks", qos: .userInitiated).async { [weak self] in
            guard let strongSelf = self else { return }
            let newBookList = strongSelf.interactor.updateBooks(strongSelf.books, havingChanges: updatedBooks)
            strongSelf.books = newBookList
            DispatchQueue.main.async {
                strongSelf.view?.reload(all: strongSelf.books)
            }
        }
    }
    
    // MARK: - Private functions
    
    private func handle(_ error: FetchError) {
        switch error {
        case .sql(let sqlError):
            handle(sqlError)
        case .backendSystem(let backendError):
            handle(backendError)
        case .invalidImage:
            handleInvalidImage()
        case .noAvailableEbooks:
            handleNoAvailableEbooks()
        case .unknown(let unknownError):
            handleUnknown(unknownError)
        }
    }
    
    private func handle(_ error: FetchError.SQL) {
        switch error {
        case .query(let queryError):
            handleUnknown(queryError)
        case .underlying(let underlyingSQLError):
            handle(underlyingSQLError)
        }
    }
    
    private func handle(_ error: FetchError.BackendSystem) {
        switch error {
        case .dropbox(let dropboxAPIError):
            handle(dropboxAPIError)
        case .contentServer(let contentServerError):
            handleContentServer(contentServerError)
        case .unconfiguredBackend:
            handleUnconfiguredBackend()
        }
    }
    
    private func handleInvalidImage() {
        // as of now, this can't happen. Will need to handle this
        // once the front end is rewritten
    }
    
    private func handleNoAvailableEbooks() {
        // this should never happen, as this error should only happen when trying to download books
        // would be better to refactor the errors to better indicate that
    }
    
    private func handleUnknown(_ error: Error) {
        view?.show(message: "An unknown error has occurred: \(error.localizedDescription)")
    }
    
    private func handle(_ error: QueryError) {
        switch error {
        case .noSuchTable(let table):
            view?.show(message: "Invalid SQL query: no such table \"\(table)\"")
        case .noSuchColumn(let column, let columns):
            view?.show(message: "Invalid SQL query: no such column \"\(column)\" in column list \"\(columns)\"")
        case .ambiguousColumn(let column, let similarColumns):
            view?.show(message: "Invalid SQL query: ambiguous column \"\(column)\" in potential matches \"\(similarColumns)\"")
        case .unexpectedNullValue(let value):
            view?.show(message: "Invalid SQL query: unexpected null value \"\(value)\"")
        }
    }
    
    private func handle(_ error: SQLite.Result) {
        switch error {
        case .error(let message, let code, _):
            view?.show(message: "SQL query or response error: \"\(message)\" (\(code))")
        }
    }
    
    private func handle(_ error: DropboxBookListService.DropboxAPIError) {
        switch error {
        case .unauthorized:
            view?.show(message: "Dropbox has been selected, but not connected. Go into settings to connect to Dropbox.")
        case .downloadError(let downloadError):
            handle(downloadError)
        case .searchError(let searchError):
            handle(searchError)
        case .noSearchResults:
            handleNoSearchResults()
        case .nonsenseResponse:
            view?.show(message: "Dropbox connectivity has encountered an unexpected error. If you are seeing this message, please contact app support.")
        case .noNetwork:
            view?.show(message: "Dropbox connectivity requires a network connection. Please connect to Wi-Fi or Cellular data.")
        }
    }
    
    private func handleContentServer(_ error: Error) {
        view?.show(bookCount: 0)
        
        if let calibreError = error as? CalibreError {
            view?.show(message: "Error: \(calibreError.localizedDescription)")
        } else {
            view?.show(message: "Error: \(error.localizedDescription) - Double check your Calibre© Content Server setup in settings (https:// or http:// is required) and make sure your server is up and running.")
        }
    }
    
    private func handleUnconfiguredBackend() {
        view?.show(message: "Go into settings to connect to Dropbox or to your content server.")
    }
    
    // swiftlint:disable:next function_body_length
    private func handle(_ error: CallError<Files.DownloadError>) {
        switch error {
        case .internalServerError(let code, let string, let string2):
            view?.show(
                message: """
                Dropbox has encountered an internal error:
                
                \(string ?? "")
                \(string2 ?? "")
                Error code \(code)
                """
            )
        case .badInputError(let string, let string2):
            view?.show(
                message: """
                Dropbox has encountered an input error:
                
                \(string ?? "")
                \(string2 ?? "")
                """
            )
        case .rateLimitError(let rateLimitError, let string, let string2, let string3):
            view?.show(
                message: """
                Dropbox has encountered an access error due to too many requests:
                
                \(rateLimitError.description)
                \(string ?? "")
                \(string2 ?? "")
                \(string3 ?? "")
                """
            )
        case .httpError(let int, let string, let string2):
            view?.show(
                message: """
                Dropbox has encountered an HTTP error:
                
                \(string ?? "")
                \(string2 ?? "")
                Error code \(int ?? 0)
                """
            )
        case .authError(let authError, let string, let string2, let string3):
            view?.show(
                message: """
                Dropbox has encountered an authentication error:
                
                \(authError.description)
                \(string ?? "")
                \(string2 ?? "")
                \(string3 ?? "")
                """
            )
        case .accessError(let accessError, let string, let string2, let string3):
            view?.show(
                message: """
                Dropbox has encountered an access error:
                
                \(accessError.description)
                \(string ?? "")
                \(string2 ?? "")
                \(string3 ?? "")
                """
            )
        case .routeError(let routeError, let string, let string2, let string3):
            view?.show(
                message: """
                Dropbox has encountered a routing error:
                
                \(routeError.unboxed.description)
                \(string ?? "")
                \(string2 ?? "")
                \(string3 ?? "")
                """
            )
        case .clientError(let clientError):
            view?.show(
                message: """
                Dropbox has encountered a client error:
                
                \(clientError?.localizedDescription ?? "")
                """
            )
        }
    }
    
    private func handle(_ error: CallError<Files.SearchError>) {
        // this should never happen, as this error should only happen when trying to download books
        // would be better to refactor the errors to better indicate that
    }
    
    private func handleNoSearchResults() {
        // this should never happen, as this error should only happen when trying to download books
        // would be better to refactor the errors to better indicate that
    }
}
