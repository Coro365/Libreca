//
//  CalibreContentServerService.swift
//  Libreca
//
//  Created by Justin Marshall on 5/7/19.
//  
//  Libreca is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  Libreca is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with Libreca.  If not, see <https://www.gnu.org/licenses/>.
//
//  Copyright © 2019 Justin Marshall
//  This file is part of project: Libreca
//

import CalibreKit

struct CalibreContentServerService {
    private let batchSize = 300
    
    func fetchBooks(start: @escaping (Result<Int, FetchError>) -> Void, progress: @escaping (_ result: BookFetchResult, _ index: Int) -> Void, completion: @escaping ([BookFetchResult]) -> Void) {
        startFetchingBooks(start: start, progress: progress, completion: completion)
    }
    
    func fetchImage(for bookID: Int, authors: [BookModel.Author], title: BookModel.Title, completion: @escaping (Result<Data, Error>) -> Void) {
        fatalError("to be implemented as part of legacy system rewrite")
    }
    
    func fetchFormat(authors: [BookModel.Author], title: BookModel.Title, format: BookModel.Format, completion: @escaping (Swift.Result<Data, Error>) -> Void) {
        fatalError("to be implemented as part of legacy system rewrite")
    }
    
    func retryFailures(from books: [BookFetchResult],
                       progress: @escaping (_ retriedResult: BookFetchResult, _ index: Int) -> Void,
                       completion: @escaping () -> Void) {
        // A better solution would be to fetch them already sorted from the server,
        // that way they populate in the UI in the right order, but this is good
        // enough for now.
        let dispatchGroup = DispatchGroup()
        
        let retries: [(Int, BookFetchResult.Retry)] = books.enumerated().compactMap {
            guard let retry = $0.element.retry else { return nil }
            return ($0.offset, retry)
        }
        
        retries.forEach { index, retry in
            dispatchGroup.enter()
            retry { retriedResult in
                progress(retriedResult, index)
                dispatchGroup.leave()
            }
        }
        
        dispatchGroup.notify(queue: .main) {
            completion()
        }
    }
    
    private func startFetchingBooks(start: @escaping (Result<Int, FetchError>) -> Void, progress: @escaping (_ result: BookFetchResult, _ index: Int) -> Void, completion: @escaping ([BookFetchResult]) -> Void) {
        Cache.clear()
        
        SearchEndpoint(count: batchSize).hitService { searchResponse in
            switch searchResponse.result {
            case .success(let value) where value.totalBookCount == 0:
                start(.success(0))
            case .success(let value):
                self.paginate(from: value, start: start, progress: progress, completion: completion)
            case .failure(let error):
                start(.failure(.backendSystem(.contentServer(error))))
            }
        }
    }
    
    private func paginate(from search: Search, start: @escaping (Result<Int, FetchError>) -> Void, progress: @escaping (_ result: BookFetchResult, _ index: Int) -> Void, completion: @escaping ([BookFetchResult]) -> Void) {
        start(.success(search.totalBookCount))
        
        nextPage(startingAt: search.bookIDs.count, totalBookCount: search.totalBookCount, bookIDs: search.bookIDs, start: start) { bookIDs in
            var allBookDetails: [BookFetchResult] = []
            let dispatchGroup = DispatchGroup()
            
            bookIDs.enumerated().forEach { index, bookID in
                dispatchGroup.enter()
                bookID.hitService { bookIDResponse in
                    switch bookIDResponse.result {
                    case .success(let book):
                        let bookFetchResult = BookFetchResult.book(book)
                        progress(bookFetchResult, index)
                        allBookDetails.append(bookFetchResult)
                        dispatchGroup.leave()
                    case .failure:
                        func retry(retryCompletion: @escaping (BookFetchResult) -> Void) {
                            bookID.hitService { bookIDResponse in
                                switch bookIDResponse.result {
                                case .success(let book):
                                    let bookFetchResult = BookFetchResult.book(book)
                                    retryCompletion(bookFetchResult)
                                case .failure:
                                    let bookFetchResult = BookFetchResult.failure(retry: retry)
                                    retryCompletion(bookFetchResult)
                                }
                            }
                        }
                        let bookFetchResult = BookFetchResult.failure(retry: retry)
                        progress(bookFetchResult, index)
                        allBookDetails.append(bookFetchResult)
                        dispatchGroup.leave()
                    }
                }
            }
            dispatchGroup.notify(queue: .main) {
                completion(allBookDetails)
            }
        }
    }
    
    private func nextPage(startingAt offset: Int,
                          totalBookCount: Int,
                          bookIDs: [BookEndpoint],
                          start: @escaping (Result<Int, FetchError>) -> Void,
                          completion: @escaping ([BookEndpoint]) -> Void) {
        var bookIDs = bookIDs
        SearchEndpoint(count: batchSize, offset: offset).hitService { nextSearchResponse in
            switch nextSearchResponse.result {
            case .success(let nextValue):
                bookIDs.append(contentsOf: nextValue.bookIDs)
                if totalBookCount != nextValue.offset {
                    self.nextPage(startingAt: bookIDs.count, totalBookCount: totalBookCount, bookIDs: bookIDs, start: start, completion: completion)
                } else {
                    completion(bookIDs)
                }
            case .failure(let error):
                start(.failure(.backendSystem(.contentServer(error))))
            }
        }
    }
}
